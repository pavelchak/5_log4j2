package com.pavelchak.Animals;

import org.apache.logging.log4j.*;

public class Cat {
    private String name;
    private static Logger logger = LogManager.getLogger(Cat.class);

    public Cat(String name) {
        this.name = name;
    }

    public void generateMessage() {
        logger.trace("This is a trace message");
        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        logger.error("This is an error message");
        logger.fatal("This is a fatal message");
    }
}
