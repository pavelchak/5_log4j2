package com.pavelchak;

import com.pavelchak.Animals.Cat;
import com.pavelchak.Animals.Dog;
import com.pavelchak.Flowers.Narcissus;
import com.pavelchak.Flowers.Rose;
import com.pavelchak.People.Man;
import com.pavelchak.People.Woman;
import org.apache.logging.log4j.*;
import java.io.IOException;

public class Application {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        //send log to mail
        Rose rose = new Rose("Olia");
        rose.generateMessage();
        //send log to sms
        Narcissus narcissus = new Narcissus("Olenka");
        narcissus.generateMessage();

        logger1.error("Exception", new IOException("HI"));

        logger1.trace("This is a trace message");
        logger1.debug("This is a debug message");
        logger1.info("This is an info message");
        logger1.warn("This is a warn message");
        logger1.error("This is an error message");
        logger1.fatal("This is a fatal message");

        Man man = new Man("Andrii");
        Woman woman = new Woman("Halia");
        Dog dog = new Dog("Barbos");
        Cat cat = new Cat("Murchyk");

        for (int i = 0; i < 2; i++) {
            man.generateMessage();
            woman.generateMessage();
            dog.generateMessage();
            cat.generateMessage();
        }
    }
}
