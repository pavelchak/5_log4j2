package com.pavelchak.Flowers;

import org.apache.logging.log4j.*;

public class Rose {
    private String name;
    private static Logger logger = LogManager.getLogger(Rose.class);

    public Rose(String name) {
        this.name = name;
    }

    public void generateMessage() {
        logger.trace("This is a trace message");
        logger.debug("This is a debug message");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        logger.error("This is an error message");
        logger.fatal("This is a fatal message");
    }

}
