package com.pavelchak.People;

import org.apache.logging.log4j.*;

public class Man {
    private String name;
    private static Logger logger = LogManager.getLogger(Man.class);

    public Man(String name) {
        this.name = name;
    }

    public void generateMessage() {
        logger.trace("This is a trace message from {}", this.name);
        logger.debug("This is a debug message from {}", this.name);
        logger.info("This is an info message from {}", this.name);
        logger.warn("This is a warn message from {}", this.name);
        logger.error("This is an error message from {}", this.name);
        logger.fatal("This is an fatal message from {}", this.name);
    }
}

