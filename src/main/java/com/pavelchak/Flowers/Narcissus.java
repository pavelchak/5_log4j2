package com.pavelchak.Flowers;

import org.apache.logging.log4j.*;

public class Narcissus {
    private String name;
    private static Logger logger = LogManager.getLogger(Narcissus.class);

    public Narcissus(String name) {
        this.name = name;
    }

    public void generateMessage() {
        logger.trace("This is a trace message for Narcissus");
        logger.debug("This is a debug message for Narcissus");
        logger.info("This is an info message for Narcissus");
        logger.warn("This is a warn message for Narcissus");
        logger.error("This is an error message for Narcissus");
        logger.fatal("This is a fatal message for Narcissus");
    }
}
