package com.pavelchak.SMS;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "AC502966f8d34a28f768d20ea2d2ebdfd5";
    public static final String AUTH_TOKEN = "e6ff9c87df85da29a3282fb9769973e2";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380677208335"),new PhoneNumber("+14355629107"), str) .create();
    }
}
